#pragma once
#include <GL/glew.h>
#include <GLFW\glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

class Triangle
{
private:
	const static GLfloat g_vertex_buffer_data[9];
public:
	Triangle();
	~Triangle();
	GLfloat getTriangleData();

	
};